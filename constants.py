# default values
# for adjustments, use set_experiment()
L = 0.2
min_L = L / 10
R = L/5
eval_time = 1200
period_min = 0
period_mp = 5
amp_var = 0.3 * 3.141592653589793  # variability
inherit_method = 0  # 0 = length and width adjustment, 1 = single point crossover, 2 = no inherit
co_select_method = 0  # 0 = fitness based, 1 = preference based, 2 = with penalty
penalty_impact = 1  # normalized value from min to max of pop times impact is reduction on F
penalty_measure = 'length'  # on which measure the penalty is imposed
mutate_rate = 0.05  # chance for each value to be mutated
pop_size = 610  # should equal pa * co * ch + ri
pa = 100  # parents, number of parents selected from pop
co = 3  # co-parents, number of co parents selected per parent
ch = 2  # children, offspring produces per parent/co parent couple
ri = 10  # random insert, how many randomly generated individuals are inserted in the new generation
tournament_size = 6  # size of the pool for which a winner is selected for each tournament
num_gens = 750
model = 2  # 0 = two headed worm, 2 = broad walker
use_aggression = True
aggression_mean = 0.5
aggression_mutate_chance = 0.2
aggression_crossover_impact = 0.5
use_size_variation = True
size_inherit_impact = 0.8
size_variation = 0.25  # how much randomness is used in the sizing of limbs
size_mutation_chance = 0.09  # chance of size attribute getting mutated for each limb
size_mutation_sd = 0.25  # standard deviation used for a generated gauss
force_mutation = False  # whether there is always at least 1 property mutated in the genome
age_relative_threshold = True

# with absolute threshold: relative change, relative t: fraction of pop
# number of reset will be rounded up
# if multiple changes are on the threshold, all will be reset
age_reset_threshold = 0.05

# number of runs per experiment
seeds_per_exp = 100

# used to save constants to output file
c_dict = {
    'L': L,
    'min_L': min_L,
    'R': R,
    'eval_time': eval_time,
    'period_min': period_min,
    'period_mp': period_mp,
    'amp_var': amp_var,
    'inherit_method': inherit_method,
    'co_select_method': co_select_method,
    'penalty_impact': penalty_impact,
    'penalty_measure': penalty_measure,
    'mutate_rate': mutate_rate,
    'pop_size': pop_size,
    'pa': pa,
    'co': co,
    'ch': ch,
    'ri': ri,
    'tournament_size': tournament_size,
    'num_gens': num_gens,
    'model': model,
    'use_aggression': use_aggression,
    'aggression_mean': aggression_mean,
    'aggression_mutate_chance': aggression_mutate_chance,
    'aggression_crossover_impact': aggression_crossover_impact,
    'use_size_variation': use_size_variation,
    'size_inherit_impact': size_inherit_impact,
    'size_variation': size_variation,
    'size_mutation_chance': size_mutation_chance,
    'size_mutation_sd': size_mutation_sd,
    'force_mutation': force_mutation,
    'age_relative_threshold': age_relative_threshold,
    'age_reset_threshold': age_reset_threshold
}


def set_experiment(exp_n):
    """

    warning:
      if value that is being mutated is in a kwarg, do an extra check

    :param exp_n:
    :return:
    """
    global c_dict
    global inherit_method
    global use_size_variation
    global co_select_method

    if exp_n == 1:
        # mutation only
        inherit_method = 2
        c_dict['inherit_method'] = inherit_method
    elif exp_n == 2:
        # crossover
        # default, so no changes
        pass
    elif exp_n == 3:
        # crossover with preference
        co_select_method = 1
        c_dict['co_select_method'] = co_select_method
    elif exp_n == 4:
        # crossover with penalty
        co_select_method = 2
        c_dict['co_select_method'] = co_select_method
    elif exp_n == 5:
        # single point crossover
        inherit_method = 1
        c_dict['inherit_method'] = inherit_method
    else:
        print "ERROR: experiment number not found:", exp_n
