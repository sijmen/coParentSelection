# Created by Sijmen van der Willik
# 17-05-17 14:09

import constants as c
import numpy as np
import math
import random


class Robot(object):
    """ Class to create a robot to be used in a pyrosim simulator

    every limb connection has a joint with angle perpendicular on the plane on which the two limbs can be placed
    every joint has a motor
    every motor is driven by a sinusoid function
    """
    def __init__(self, sim, genome):
        """
        limb_ids:
            dictionary holding all the ids associated with the attached limbs

        :param sim: <pyrosim simulator>
        :param genome: <dict> see description in individual.py
        """
        self.sim = sim
        self.morphology = {0: {
            0: {'x': 0, 'y': -0.5*c.L, 'z': c.R},
            1: {'x': 0, 'y': 0.5*c.L, 'z': c.R},
            'dir': [0, 1, 0]}
        }

        self.tau = genome['tau']

        self.limb_ids = {}
        self.last_id = -1
        self.measures = {''}

        # build the morphology
        for limb in genome['morphology']:
            attach_at = self.get_attach_point(limb)
            own_transform = self.get_own_transform(limb)
            self.add_to_morphology(attach_at, own_transform, limb['dir'], limb_id=limb['id'])

        height_shift = self.place_on_ground()

        # build the robot, start with base cylinder
        self.sim.send_cylinder(x=0, y=0, z=c.R+height_shift,
                               length=c.L, radius=c.R,
                               r1=0, r2=1, r3=0,
                               r=0, g=1, b=0)

        # add a sensor to measure the position of the robot
        self.sim.send_position_sensor(body_id=0)

        for limb in genome['morphology']:
            self.add_limb(limb)

    def add_limb(self, limb):
        """Takes a dictionary describing a limb as input, adds the objects and necessary other parts

        :param limb: <dict> i.e.:
            {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 1, 0], 'rgb': [1, 1, 1]}
            id: <int> id of the limb
            parent: <int> id of the parent limb
            side: <int> which side it should be attached to, 0 is closer to the body
            dir: <array of floats or ints> describing the initial direction of the limb
            rgb: <array of floats or ints> color of the object
        :return:
        """
        if limb['id'] not in self.limb_ids:
            attach_at = self.get_attach_point(limb)
            own_transform = self.get_own_transform(limb)
            transform_limb = attach_at + own_transform

            self.create_limb(limb, transform_limb)
            self.attach_limb(limb, attach_at)
        else:
            print "Warning: trying to add limb with duplicate id:", limb['id']

    def add_to_morphology(self, attach_at, own_transform, direction, limb_id=None):
        """ adds the last created limb to the morphology dict of the robot or the kwarg id

        :param attach_at: <dict>
        :param own_transform: <array of floats or ints> states the necessary transform to put the limb into place
        :param direction: <array of floats or ints> direction of the limb
        :param limb_id: <int> uses this id instead of the last entered
        :return:
        """
        if limb_id is None:
            limb_id = self.limb_ids[-1]

        low = attach_at
        high = attach_at + 2 * own_transform
        self.morphology[limb_id] = {
            0: {'x': low[0], 'y': low[1], 'z': low[2]},
            1: {'x': high[0], 'y': high[1], 'z': high[2]},
            'dir': direction
        }

    def attach_limb(self, limb, attach_at):
        """attaches the limb to the body by creating a joint with a motor neuron

        a sinusoid function is then connected to the motor using a neuron and a synapse

        :param limb: <dict> see add_limb
        :param attach_at: coordinates of attachment point
        :return:
        """
        parent = self.morphology[limb['parent']]

        if 'joint_dir' not in limb:
            join_dir = np.cross(limb['dir'], parent['dir'])
            if join_dir[0] == 0 and join_dir[1] == 0 and join_dir[2] == 0:
                print "FATAL ERROR: vector with [0, 0, 0] found."
        else:
            join_dir = limb['joint_dir']

        return_id = self.sim.send_hinge_joint(first_body_id=limb['parent'], second_body_id=limb['id'],
                                              x=attach_at[0], y=attach_at[1], z=attach_at[2],
                                              n1=join_dir[0], n2=join_dir[1], n3=join_dir[2],
                                              lo=-math.pi / 2, hi=math.pi / 2
                                              )

        self.limb_ids[limb['id']]['joint'] = return_id

        motor_id = self.sim.send_motor_neuron(joint_id=return_id, tau=self.tau)

        self.limb_ids[limb['id']]['motor_neuron'] = motor_id

        # connecting the sinusoid to the motor neuron
        oscillator = self.create_oscillator(limb['sin']['offset'], limb['sin']['amp'], limb['sin']['period'])
        return_id = self.sim.send_function_neuron(function=oscillator)

        self.limb_ids[limb['id']]['function_neuron'] = return_id

        self.sim.send_synapse(source_neuron_id=return_id, target_neuron_id=motor_id, weight=1.0)

    def create_limb(self, limb, transform):
        """Creates the actual object in the sim

        :param limb: <dict> describing the limb, (see add_limb function for details)
        :param transform: <vector> describing where the limb should be placed in 3d space
        :return:
        """
        self.limb_ids[limb['id']] = {}

        if 'rgb' not in limb:
            limb['rgb'] = [random.random(), random.random(), random.random()]

        return_id = self.sim.send_cylinder(x=transform[0], y=transform[1], z=transform[2],
                                           length=limb['size'] * c.L, radius=c.R,
                                           r1=limb['dir'][0], r2=limb['dir'][1], r3=limb['dir'][2],
                                           r=limb['rgb'][0], g=limb['rgb'][1], b=limb['rgb'][2])

        self.limb_ids[limb['id']]['body'] = return_id

    @staticmethod
    def create_oscillator(offset, amplitude, period):
        """ function used to generate a sinusoid function

        :param period: period of the function
        :param offset: shift over x-axis
        :param amplitude:
        :return:
        """
        amplitude = amplitude * c.amp_var
        period = period * c.period_mp + c.period_min
        offset = offset * period

        return lambda t: amplitude * (math.sin((math.pi / period) * (t - offset)))

    def get_attach_point(self, limb):
        """limb to be attached

        :param limb:
        :return: numpy vector with xyz of attach point
        """
        parent_id = limb['parent']
        side = limb['side']
        xyz = np.array([self.morphology[parent_id][side]['x'], self.morphology[parent_id][side]['y'], self.morphology[parent_id][side]['z']])
        return xyz

    def get_measures(self, include_height=False):
        """Calculates the measures from the current robot and returns them

        current measures to calculate:
        length - distance from attach point with highest y to the one with the lowest y
        width - distance from attach point with highest x to the one with the lowest x
        aggression - tau

        :return:
        """
        result = {}

        # length and width
        min_x = 9999999
        max_x = -9999999
        min_y = 9999999
        max_y = -9999999

        if include_height:
            min_z = 9999999
            max_z = -9999999

        for limb in self.morphology:
            for i in range(0, 2):
                tmp = self.morphology[limb][i]
                if tmp['x'] < min_x:
                    min_x = tmp['x']
                elif tmp['x'] > max_x:
                    max_x = tmp['x']

                if tmp['y'] < min_y:
                    min_y = tmp['y']
                elif tmp['y'] > max_y:
                    max_y = tmp['y']

                if include_height:
                    if tmp['z'] < min_z:
                        min_z = tmp['z']
                    elif tmp['z'] > max_z:
                        max_z = tmp['z']

        result['length'] = max_y - min_y
        result['width'] = max_x - min_x

        if include_height:
            result['height'] = max_z - min_z

        # aggression
        if c.use_aggression:
            result['aggression'] = self.tau

        return result

    @staticmethod
    def get_own_transform(limb):
        """Returns the transformation needed to place an object with one side on [0, 0, 0]
        pointing away in dir as stated in limb dict

        :param limb: <dict>  describing the limb, (see add_limb function for details)
        :return: <vector> the transformation
        """
        direction = np.array(limb['dir'])

        # pythagoras
        # solve for:
        # sqrt( (d0*x)2 + (d1*x)2 + (d2*x)2 ) == L2
        multiplier = direction[0]**2 + direction[1]**2 + direction[2]**2
        ls = (c.L * limb['size'])**2
        xs = ls / multiplier
        x = np.sqrt(xs)

        return direction * 0.5 * x

    def place_on_ground(self):
        """Adjusts the z-values of all limbs to make sure nothing is below the ground

        since the radius applies to any object, the minimum z-value after moving should be c.R
            c.R is the radius as defined in constants.py

        :return: the amount by which was shifted
        """

        min_z = 9999

        for limbKey in self.morphology:
            if self.morphology[limbKey][0]['z'] < min_z:
                min_z = self.morphology[limbKey][0]['z']
            if self.morphology[limbKey][1]['z'] < min_z:
                min_z = self.morphology[limbKey][1]['z']

        transform = c.R - min_z

        for limbKey in self.morphology:
            self.morphology[limbKey][0]['z'] += transform
            self.morphology[limbKey][1]['z'] += transform

        return transform

    def print_morphology(self):
        """Pretty prints the morphology of the robot

        :return:
        """
        for limbKey in self.morphology:
            print
            print "limb:"
            for key in self.morphology[limbKey]:
                print key, ":", self.morphology[limbKey][key]

        for limb_id in self.limb_ids:
            print
            print "limb:", limb_id
            for key in self.limb_ids[limb_id]:
                print key, ":", self.limb_ids[limb_id][key]


if __name__ == "__main__":
    """Creates a robot and runs the simulator
    """
    import pyrosim
    import random
    testGenome = {
        'morphology': [
            {'id': 1, 'parent': 0, 'side': 1, 'size': 1,
             'dir': [random.random()*2-1, random.random()*2-1, random.random()*2-1],
             'rgb': [random.random(), random.random(), random.random()],
             'sin': {'offset': 0, 'amp': 1, 'period': 1}},
            {'id': 2, 'parent': 1, 'side': 1, 'size': 1,
             'dir': [random.random()*2-1, random.random()*2-1, random.random()*2-1],
             'rgb': [random.random(), random.random(), random.random()],
             'sin': {'offset': 0.2, 'amp': 1, 'period': 1}},
            {'id': 3, 'parent': 0, 'side': 0, 'size': 1,
             'dir': [random.random()*2-1, random.random()*2-1, random.random()*2-1],
             'rgb': [random.random(), random.random(), random.random()],
             'sin': {'offset': 0.4, 'amp': 1, 'period': 1}},
            {'id': 4, 'parent': 3, 'side': 1, 'size': 1,
             'dir': [random.random()*2-1, random.random()*2-1, random.random()*2-1],
             'rgb': [random.random(), random.random(), random.random()],
             'sin': {'offset': 0.6, 'amp': 1, 'period': 1}},
        ],
        'tau': 0.1,
        'preferences': {'height': 1}
    }

    testSim = pyrosim.Simulator(play_paused=False, eval_time=500, play_blind=False)
    robot = Robot(testSim, testGenome)
    robot.print_morphology()
    testSim.start()
