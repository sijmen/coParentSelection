# Created by Sijmen van der Willik
# 18-05-17 14:13

import os
import sys

import json
import datetime
import pickle
import random
import numpy as np

import constants as c
from population import Population
import timeit
import matplotlib.pyplot as plt
start_time = timeit.default_timer()

show_eta = True
verbose = False

sys_argument = c.seeds_per_exp + 1

try:
    sys_argument = sys.argv[1]
    sys_argument = int(sys_argument)
    print "System argument found:", sys_argument
except:
    print "No system argument provided, using default:", sys_argument

exp_n = (sys_argument-1) / c.seeds_per_exp + 1
seed = sys_argument % c.seeds_per_exp
if seed == 0:
    seed = c.seeds_per_exp

random.seed(seed)
np.random.seed(seed)

c.set_experiment(exp_n)

print "Experiment:", exp_n, "seed:", seed

statistics = {
    'best': [],
    'worst': [],
    'median': [],
    'average': [],
    'diffs': [],
    'ages': []
}
genome_list = []

# init
parents = Population(c.pop_size)
parents.initialize()
parents.randomize()
parents.evaluate(False, True)

[max_v, min_v, med_v, avg_v, diffs_v, ages] = parents.get_stats()
statistics['best'].append(max_v)
statistics['worst'].append(min_v)
statistics['median'].append(med_v)
statistics['average'].append(avg_v)
statistics['diffs'].append(diffs_v)
statistics['ages'].append(ages)

best_genome = parents.get_best()[0]
genome_list.append(best_genome)

if verbose:
    print "Gen #", 0,
    parents.print_self()

# timing
if show_eta:
    gen_timer = timeit.default_timer()

for i in range(0, c.num_gens):
    if show_eta and i % 10 == 0 and i > 0:
        previous = gen_timer
        gen_timer = timeit.default_timer()
        diff = gen_timer - previous
        gen_left = c.num_gens - i
        print "Time left:", diff / 10 * gen_left, "Time last 10:", diff, "gen left:", gen_left

    # initialize new population
    children = Population(c.pop_size)

    # select the parents and create offspring
    children.fill_from(parents)

    children.mutate()
    children.evaluate(False, True)

    # create full population having both and reduce to original size using NSGA-II
    children.adjust_age()
    children.merge_parents(parents)

    parents = children

    [max_v, min_v, med_v, avg_v, diffs_v, ages] = parents.get_stats()
    statistics['best'].append(max_v)
    statistics['worst'].append(min_v)
    statistics['median'].append(med_v)
    statistics['average'].append(avg_v)
    statistics['diffs'].append(diffs_v)
    statistics['ages'].append(ages)

    best_genome = parents.get_best()[0]
    genome_list.append(best_genome)

    # print generation number
    if verbose:
        print "Gen #", (i + 1),
        parents.print_self()

stop_time = timeit.default_timer()
runtime_str = "runtime:" + str(stop_time - start_time)
print runtime_str

# save results
best_genome, best_fitness = parents.get_best()

timestamp = datetime.datetime.now().strftime("%m%d_%H%M%S")
fitness = int(round(best_fitness*1000))
fitness = str(fitness).zfill(4)
run_name = 'exp' + str(exp_n) + '_' + timestamp + '_' + str(seed) + '_f' + fitness

output_dir = ""
if os.path.isdir("output"):
    output_dir = "output/"

# create output directory
if not os.path.exists(output_dir+run_name):
    os.makedirs(output_dir+run_name)

with open(output_dir+run_name + '/' + 'constants_'+timestamp+'.txt', 'w') as outfile:
    json.dump(c.c_dict, outfile)

with open(output_dir + run_name + '/' + 'other_' + timestamp + '.txt', 'w') as outfile:
    outfile.write(runtime_str + '\nseed:' + str(seed))

with open(output_dir+run_name + '/' + 'statistics_'+timestamp+'.txt', 'w') as outfile:
    pickle_str = pickle.dumps(statistics)
    outfile.write(pickle_str)

with open(output_dir+run_name + '/' + 'genomes_'+timestamp+'.txt', 'w') as outfile:
    pickle_str = pickle.dumps(genome_list)
    outfile.write(pickle_str)

plt.plot(statistics['diffs'], 'c.')
plt.plot(statistics['best'], 'g')
plt.plot(statistics['worst'], 'r')
plt.plot(statistics['median'], 'b--')
plt.plot(statistics['average'], 'b*')
plt.ylabel('some numbers')
plt.show()
