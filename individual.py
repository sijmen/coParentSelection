# Created by Sijmen van der Willik
# 18-05-17 12:43

import random
import math
import copy
import numpy as np
import pyrosim
from robot import Robot
import constants as c


class Individual:
    """An individual in the population, holds the genome and combination and mutation functions

    """
    def __init__(self, i):
        """

        ID              <int> the id of the individual
        genome          <dict> the genome
            morphology  <array of dicts> has one entry for each limb attached to the robot
                id      <int> id of the limb
                parent  <int> the parent limb to which the limb is connected to
                side    <int> on which side the limb is attached, 0 is towards the body
                size    <float> size, relative to length as defined by L in constants.py
                dir     <numpy array> the direction of a limb, given as a 3d vector
                rgb     <numpy array> the color of the limb, as rgb values
                sin     <dict> holding the variables used to create a sinusoid function
                    offset <float, int> shift over x-axis (time)
                    amp    <float, int> amplitude of the sinusoid
                    period <float, int> period of the sinusoid
            tau         <float> the tau value used for the limbs in the robot
            preferences <dict> holds the preferences of the robot regarding attributes of co-parents
        fitness         <float> the fitness value of the individual

        :param i: the id of the individual
        """
        self.id = i
        self.age = 0
        self.genome = {
            'morphology': [],
            'tau': 1,
            'preferences': {'length': -1, 'width': 0, 'aggression': 0},
            'measures': {'length': 0, 'width': 0, 'aggression': 0}
        }
        self.fitness = 0
        self.parent_fitness = 0
        self.parent_measures = {'length': 0, 'width': 0}
        self.no_limbs = 0
        self.robot = -1

        self.fill_model()

    def adjust_age(self):
        """Checks the change in morphology the parent morphology,
        if the change is greater than the set threshold, age will be reset to 0

        :return: <bool> whether the age was adjusted
        """
        # don't check if no parents exist
        if self.parent_measures['length'] != 0:
            change = self.get_size_change()

            if change > c.age_reset_threshold:
                self.age = 0
        else:
            print "WARNING: parent found with length 0"

    def start_evaluation(self, pp, pb, wait=False, eval_time=c.eval_time, capture=False):
        """Starts the evaluation process of the individual in the simulator,
        compute fitness must be called in order to obtain the fitness resulting from the simulation

        :param pp: <bool> play paused, whether the simulation starts in paused mode
        :param pb: <bool> play blind, whether the simulation window is shown
        :param wait: <bool> whether this function should wait for the simulation to end
        :param eval_time: <int> number of time steps for the simulation
        :param capture: <bool> whether the frames of the simulation should be captured
        :return:
        """
        self.sim = pyrosim.Simulator(play_paused=pp, eval_time=eval_time, play_blind=pb, capture=capture)
        self.robot = Robot(self.sim, self.genome)
        self.sim.start()

        if wait:
            self.sim.wait_to_finish()

    def compute_fitness(self):
        """computes the fitness of the individual

        the fitness of the individual is equal to the distances traveled along the y-axis and z-axis
        the distance is measured with a position sensor on the body with id 0

        the fitness is the sum of the distance in both directions

        a distance greater than 20 is treated as an explosion and will be given fitness 0

        :return:
        """
        self.sim.wait_to_finish()

        y = self.sim.get_sensor_data(sensor_id=0, svi=1)
        z = self.sim.get_sensor_data(sensor_id=0, svi=2)

        y = y[-1] - y[0]
        z = z[-1] - z[0]

        self.fitness = y + z

        if self.fitness > 20:
            self.fitness = 0

        self.set_measures()

        del self.sim

    def fill_model(self, model=c.model, use_size_variation=None):
        """Creates the morphology that fits a certain pre designed model

        :return:
        """
        if use_size_variation is None:
            use_size_variation = c.use_size_variation

        if use_size_variation:
            min_size = 1 - c.size_variation
            mp = c.size_variation * 2
        else:
            min_size = 1
            mp = 0

        if model == 0:
            # two headed worm
            self.no_limbs = 3
            self.genome['morphology'] = [
                {'id': 1, 'parent': 0, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [1, 1, 0],
                 'joint_dir': [-1, -1, 0],
                 'sin': {'offset': 0, 'amp': 1, 'period': 1}},
                {'id': 2, 'parent': 0, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [-1, 1, 0],
                 'joint_dir': [1, 1, 0],
                 'sin': {'offset': 0.2, 'amp': 1, 'period': 1}},
                {'id': 3, 'parent': 0, 'side': 0, 'size': random.random() * mp + min_size,
                 'dir': [0, -1, 0],
                 'joint_dir': [-1, 0, 0],
                 'sin': {'offset': 0.4, 'amp': 1, 'period': 1}}
            ]
        elif model == 1:
            # quadruped, not in use
            pass

        elif model == 2:
            # broad walker
            self.no_limbs = 9
            self.genome['morphology'] = [
                {'id': 1, 'parent': 0, 'side': 0, 'size': random.random() * mp + min_size,
                 'dir': [0, -1, 0],
                 'joint_dir': [0, 0, 1],
                 'sin': {'offset': 0, 'amp': 1, 'period': 1},
                 'rgb': [1, 0, 0]},
                {'id': 2, 'parent': 1, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [1, 0, 0],
                 'joint_dir': [0, 0, 1],
                 'sin': {'offset': 0.2, 'amp': 1, 'period': 1},
                 'rgb': [0, 0, 1]},
                {'id': 3, 'parent': 2, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [0, 0, -1],
                 'joint_dir': [0, 1, 0],
                 'sin': {'offset': 0.4, 'amp': 1, 'period': 1},
                 'rgb': [1, 1, 1]},
                {'id': 4, 'parent': 1, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [-1, 0, 0],
                 'joint_dir': [0, 0, 1],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [0, 0, 1]},
                {'id': 5, 'parent': 4, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [0, 0, -1],
                 'joint_dir': [0, 1, 0],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [1, 1, 1]},
                {'id': 6, 'parent': 0, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [-1, 0, 0],
                 'joint_dir': [0, 0, 1],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [0, 0, 1]},
                {'id': 7, 'parent': 6, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [0, 0, -1],
                 'joint_dir': [0, 1, 0],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [1, 1, 1]},
                {'id': 8, 'parent': 0, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [1, 0, 0],
                 'joint_dir': [0, 0, 1],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [0, 0, 1]},
                {'id': 9, 'parent': 8, 'side': 1, 'size': random.random() * mp + min_size,
                 'dir': [0, 0, -1],
                 'joint_dir': [0, 1, 0],
                 'sin': {'offset': 0.6, 'amp': 1, 'period': 1},
                 'rgb': [1, 1, 1]},
            ]
        else:
            print "WARNING: trying to add unknown model:", model

    def get_size_change(self):
        """Calculates the relative size change compared to the parent

        :return: <float> relative change
        """
        if self.parent_measures['width'] != 0:
            width_change = self.genome['measures']['width'] / self.parent_measures['width']
        else:
            width_change = 1

        if self.parent_measures['length'] != 0:
            length_change = self.genome['measures']['length'] / self.parent_measures['length']
        else:
            length_change = 1

        change = abs(width_change - 1) + abs(length_change - 1)

        return change

    def get_preference_score(self, preferences, extremes):
        """Calculates a score of the measures according to the given preferences

        :param preferences: the preferences to be used for scoring
        :param extremes: the extreme values for each measure
        :return:
        """
        score = 0

        for measure in self.genome['measures']:
            norm_value = (self.genome['measures'][measure] - extremes[measure][0]) / (extremes[measure][1] * 1.0)
            pref_val = preferences[measure]
            tmp_score = norm_value * pref_val

            score += tmp_score

        return score

    def random_init(self, model=c.model, use_size_variation=None):
        """initialized a new individual with random values

        :return:
        """
        if use_size_variation is None:
            use_size_variation = c.use_size_variation

        self.fill_model(model=model, use_size_variation=use_size_variation)
        self.randomize_sinusoids()
        self.randomize_tau()

    def randomize_sinusoids(self):
        """Puts a random value for each parameter for the sinusoid for each limb

        amp is always between -0.5pi and 0.5pi (90 degrees)
        period is always between 10 and 20
        offset is always between 0 and 1 times the period

        :return:
        """
        for limb in self.genome['morphology']:
            limb['sin']['amp'] = random.random()
            limb['sin']['period'] = random.random()
            limb['sin']['offset'] = random.random()

    def randomize_tau(self):
        self.genome['tau'] = self.generate_gauss(c.aggression_mean)

    def set_measures(self):
        """Sets the measures for this individual according to the genome

        :return:
        """
        self.genome['measures'] = self.robot.get_measures()

    def inherit(self, genome1, genome2, method=None):
        """ Combines genome1 and genome2 and applies it to self

        multipliers l_mp and w_mp are used to divide the total change over all limbs in that direction

        methods:
          0: crossover averaging genome1 and genome2
          1: single point crossover, cutting at a random point

        :param genome1: <dict>
        :param genome2: <dict>
        :param method: <int> optional, which method should be used
        :return:
        """
        if method is None:
            method = c.inherit_method

        self.genome = copy.deepcopy(genome1)

        if method == 0:
            # adjust genome1 towards measures of genome2
            l_change = genome2['measures']['length'] - genome1['measures']['length']
            w_change = genome2['measures']['width'] - genome1['measures']['width']

            # normalize
            l_change = l_change / c.L
            w_change = w_change / c.L

            # length
            if c.model == 0:
                l_limbs = [0, 1, 2]
                w_limbs = [0, 1, 2]
                l_mp = 1.0
                w_mp = 1.0
            elif c.model == 2:
                l_limbs = [0]
                w_limbs = [1, 3, 5, 7]
                l_mp = 1.0
                w_mp = 0.5
            else:
                print "Warning: cannot find limb numbers for model number:", c.model
                return False

            for limb_i in l_limbs:
                self.genome['morphology'][limb_i]['size'] += l_change * c.size_inherit_impact * l_mp

                if self.genome['morphology'][limb_i]['size'] < c.min_L:
                    self.genome['morphology'][limb_i]['size'] = c.min_L

            for limb_i in w_limbs:
                self.genome['morphology'][limb_i]['size'] += w_change * c.size_inherit_impact * w_mp

                if self.genome['morphology'][limb_i]['size'] < c.min_L:
                    self.genome['morphology'][limb_i]['size'] = c.min_L

            if c.use_aggression:
                tau_change = genome2['tau'] - genome1['tau']
                self.genome['tau'] += tau_change * c.aggression_crossover_impact

        elif method == 1:
            # single point crossover
            cross_at = random.randint(0, self.no_limbs)
            self.genome['morphology'] = self.genome['morphology'][0:cross_at] + genome2['morphology'][cross_at:]

    def mutate(self, mutate_rate=c.mutate_rate, use_size_variation=None,
               use_aggression=c.use_aggression):
        """Mutates a random value for the sinusoid in 1 random limb

        mutates the size of a limb

        :return:
        """
        if use_size_variation is None:
            use_size_variation = c.use_size_variation

        # oscillators
        tmp = np.random.rand(self.no_limbs*3)
        mutation_no = np.sum(tmp < mutate_rate)
        if mutation_no == 0 and c.force_mutation:
            mutation_no = 1
        selected_is = random.sample(range(self.no_limbs*3), mutation_no)

        for i in selected_is:
            mutate_type = i % 3
            limb_i = i / 3
            if mutate_type == 0:
                tmp = self.genome['morphology'][limb_i]['sin']['offset']
                self.genome['morphology'][limb_i]['sin']['offset'] = self.generate_gauss(tmp)
            elif mutate_type == 1:
                tmp = self.genome['morphology'][limb_i]['sin']['amp']
                self.genome['morphology'][limb_i]['sin']['amp'] = self.generate_gauss(tmp)
            elif mutate_type == 2:
                tmp = self.genome['morphology'][limb_i]['sin']['period']
                self.genome['morphology'][limb_i]['sin']['period'] = self.generate_gauss(tmp)
            else:
                print "WARNING: did not find mutation type"

        # sizing
        if use_size_variation:
            tmp = np.random.rand(self.no_limbs)
            mutation_no = np.sum(tmp < c.size_mutation_chance)
            if mutation_no == 0 and c.force_mutation:
                mutation_no = max(mutation_no, 1)
            selected_is = random.sample(range(self.no_limbs), mutation_no)

            for limb_i in selected_is:
                tmp = self.genome['morphology'][limb_i]['size']

                # use 0.5 as median because gaussian functions returns between 0 and 1
                new = self.generate_gauss(0.5, sd=c.size_mutation_sd)
                new -= 0.5
                new *= c.L

                self.genome['morphology'][limb_i]['size'] = tmp + new

        # aggression
        if use_aggression:
            if random.random() < c.aggression_mutate_chance or c.force_mutation:
                tmp = self.genome['tau']
                new = self.generate_gauss(0.5, sd=0.01)
                self.genome['tau'] = new / 0.5 * tmp

    @staticmethod
    def generate_gauss(median, sd=None):
        """generates a new value from a normal distribution

        expects a value with median between 0 and 1

        return a value between 0 and 1

        :param median:
        :param sd:
        :return:
        """
        if sd is None:
            sd = median / 20

        ret = random.gauss(median, sd)

        if ret > 1:
            ret = 1
        elif ret < 0:
            ret = 0

        return ret

    def print_fitness(self):
        print '[', self.id, self.fitness, '(', self.age, ') ] ',


if __name__ == "__main__":
    import pyrosim
    import random
    testIndividual = Individual(0)

    testIndividual.fill_model()
    testIndividual.randomize_sinusoids()

    testIndividual.start_evaluation(True, False)

    testIndividual.compute_fitness()
    testIndividual.print_fitness()
