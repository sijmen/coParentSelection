# Created by Sijmen van der Willik
# 8-8-17 11:17

import numpy as np
import matplotlib.pyplot as plt
import timeit
import copy


def get_ranks(population):
    """Calculates the pareto rank for each individual from the input

    treats every value as a cost

    :param population: <np-array of arrays of length 2>
    :return: <array of ints> list of length of outer input array with the pareto ranks of each individual
    """
    # starting rank
    rank = 1

    # init vars
    pop_size = len(population)
    ret = np.zeros(pop_size)
    indices = np.arange(pop_size)

    # continue until all ranks are calculated
    while len(population) > 0:
        cur_pop_size = len(population)
        result = np.array(np.zeros(cur_pop_size) < 1)

        for i, row in enumerate(population):
            # returns True if individual is pareto efficient
            semi_efficient = np.all(np.any(population >= row, axis=1))
            has_max_1 = np.min(population[np.where(population[:, 0] == row[0])][:, 1]) == row[1]
            has_max_2 = np.min(population[np.where(population[:, 1] == row[1])][:, 0]) == row[0]
            result[i] = semi_efficient and has_max_1 and has_max_2

        # calculate position in pop and set the rank to current rank
        for i in range(0, cur_pop_size):
            if result[i]:
                index = indices[i]
                ret[index] = rank

        # remove rank 1's from pop
        delete_these = np.where(result)
        population = np.delete(population, delete_these, axis=0)
        indices = np.delete(indices, delete_these)

        # increment the rank for next loop
        rank += 1

    return ret


def get_crowding_distances(population):
    """Calculates the crowding distance for each point in the population

    expects sorted input

    because of normalizing, the maximum crowding distance is 2,
      when both max/min are hit in both directions

    :param population: <array of arrays of length 2>
    :return: <array> list of crowding distances
    """
    population = copy.deepcopy(population)
    pop_length = len(population)
    result = np.zeros(pop_length)

    # normalize the population
    min_x = np.min(population[:, 0])
    max_x = np.max(population[:, 0])
    min_y = np.min(population[:, 1])
    max_y = np.max(population[:, 1])

    if max_x != min_x:
        population[:, 0] = (population[:, 0] - min_x) / (max_x - min_x)

    if max_y != min_y:
        population[:, 1] = (population[:, 1] - min_y) / (max_y - min_y)

    for i in range(0, pop_length):
        if i == 0 or i == pop_length - 1:
            result[i] = 999999
        else:
            result[i] = get_crowding_distance(population[i-1], population[i+1])

    return result


def get_crowding_distance(ind_1, ind_3):
    """returns the crowding distance of the individual between the two input individuals

    :param ind_1: <array of length 2>
    :param ind_3: <array of length 2>
    :return: <float> crowding distance
    """

    return abs(ind_3[0]-ind_1[0] + ind_3[1]-ind_1[1])


if __name__ == "__main__":
    start_time = timeit.default_timer()

    size = 600

    def plot_pop(population, ranks):
        colors = ['yo', 'r*', 'g.', 'b-', 'c--']

        for i in range(0, len(population)):
            plt.plot(population[i][0], population[i][1], colors[ranks[i] % len(colors)])

        plt.show()

    gen_population = np.random.rand(size, 2)

    gen_ranks = get_ranks(gen_population)

    stop_time = timeit.default_timer()
    runtime_str = "runtime:" + str(stop_time - start_time)
    print runtime_str

    plot_pop(gen_population, gen_ranks.astype(int))

