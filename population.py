# Created by Sijmen van der Willik
# 18-5-17 14:40

import copy
import random
import numpy as np

import constants as c
import pareto_ranker
from individual import Individual


class Population:
    def __init__(self, pop_size):
        """ Population class used to hold a population of individuals

        p                   <dict of Individuals, ints as keys> the population
        pop_size            <int> size of the population
        measure_extremes    <dict of arrays of floats> holding the min and max value for each measure

        :param pop_size: <int> size of the population
        """
        self.p = {}
        self.pop_size = pop_size
        self.measure_extremes = {}

    def initialize(self):
        """Creates a population of empty individuals of size pop_size

        :return:
        """
        for i in range(0, self.pop_size):
            self.p[i] = Individual(i)

    def evaluate(self, pp, pb):
        """Evaluates the entire population

        :param pp: <bool> if True, play paused is enabled
        :param pb: <boo> if True, play blind is enabled
        :return:
        """

        for i in self.p:
            self.p[i].start_evaluation(pp, pb)

        for i in self.p:
            self.p[i].compute_fitness()

        self.get_measure_extremes()

    def adjust_age(self):
        """Checks the change in morphology for each individual compared to their parent morphology,
        if the change is greater than the set threshold, age will be reset to 0

        :return:
        """
        if c.age_relative_threshold:
            changes = np.zeros(c.pop_size, dtype=float)

            for i in range(0, self.pop_size):
                changes[i] = self.p[i].get_size_change()

            index = int(c.pop_size * (1 - c.age_reset_threshold))

            threshold = np.sort(changes)[index]

            reset_idx = np.where(changes >= threshold)[0]

            for i in reset_idx:
                self.p[i].age = 0
        else:
            for i in range(0, self.pop_size):
                self.p[i].adjust_age()

    @staticmethod
    def combine_parents(parent, co_parent, new_id):
        """ creates offspring using two parents

        :param parent: <INDIVIDUAL>
        :param co_parent: <INDIVIDUAL>
        :param new_id: <int>
        :return: <INDIVIDUAL> combined parents
        """
        child = Individual(new_id)
        child.age = parent.age + 1
        child.parent_fitness = parent.fitness
        child.parent_measures['length'] = parent.genome['measures']['length']
        child.parent_measures['width'] = parent.genome['measures']['width']

        genome1 = copy.deepcopy(parent.genome)
        if new_id == 0:
            child.genome = genome1
            return child

        genome2 = copy.deepcopy(co_parent.genome)
        child.inherit(genome1, genome2)

        return child

    def fill_from(self, other, print_parents=False):
        """Fills the population using the other population as parents

        inherit the current extremes of the measures to be able to use them for co parent selection

        :param other: <POPULATION>
        :param print_parents: <bool> prints selected parents if True
        :return:
        """
        self.measure_extremes = copy.deepcopy(other.measure_extremes)

        parent_co_parent_list = self.get_parent_couples(other)
        if print_parents:
            print "selected parents:", parent_co_parent_list
        self.fill_from_couples(parent_co_parent_list, other)

    def fill_from_couples(self, couples, other):
        """Fills population by using a couples list and another population

        any remaining spots will be filled with new random generated individuals

        :param couples: array of tuples indicating couple ids
        :param other: <POPULATION> population to which the parent ids point
        :return:
        """
        count = 0
        for couple in couples:
            # select parents from couples
            parent = other.p[couple[0]]
            co_parent = other.p[couple[1]]

            # create ch (constant) many children with each couple
            for i in range(0, c.ch):
                self.p[count] = self.combine_parents(parent, co_parent, count)
                count += 1

        # fill the rest of the population with new random generated individuals
        for i in range(c.pop_size-c.ri, c.pop_size):
            self.p[i] = Individual(i)
            self.p[i].random_init()

    @staticmethod
    def get_fittest(other):
        """ gets the index of fittest individual of input pop

        :param other: <POPULATION>
        :return: <int> index of fittest individual of input pop
        """
        max_i = -1
        max_f = -1
        for i in other.p:
            if other.p[i].fitness > max_f:
                max_f = other.p[i].fitness
                max_i = i

        return max_i

    def get_measure_extremes(self):
        """Gets the minimum and maximum for each measure from the population and saves it in
          self.measure_extremes

        :return:
        """
        measures = copy.deepcopy(self.p[0].genome['measures'])

        for measure in measures:
            measures[measure] = [99999, -99999]

        # loop through population and get the measures
        for i in range(0, c.pop_size):
            for measure in measures:
                if self.p[i].genome['measures'][measure] < measures[measure][0]:
                    measures[measure][0] = self.p[i].genome['measures'][measure]
                elif self.p[i].genome['measures'][measure] > measures[measure][1]:
                    measures[measure][1] = self.p[i].genome['measures'][measure]

        self.measure_extremes = measures

    def get_parent_couples(self, other):
        """ selects parents and their co parents in couples

        the first parent will always be the fittest

        :param other:
        :return: array of tuples of ids of parents
            i.e. [(5,4), (5,3), (2,3), (2,5)]
        """
        parents = [self.get_fittest(other)]
        for i in range(1, c.pa):
            parents.append(self.winner_of_tournament(other, tournament_type=0))

        # for every parent, select co parents
        couples = []
        for parent in parents:
            existing_co_parents = []
            for i in range(0, c.co):

                # get preferences if necessary
                preferences = None
                if c.co_select_method == 1:
                    preferences = other.p[parent].genome['preferences']

                co_parent = self.select_co_parent(parent, existing_co_parents, other, preferences=preferences)
                couples.append((parent, co_parent))
                existing_co_parents.append(co_parent)

        return couples

    def get_stats(self):
        """Retrieves the stats for the current populations

        note:
            Population must be evaluated before this function is called

        :return:
        """
        values = []
        ages = np.zeros(c.pop_size, dtype=int)
        diffs = np.zeros(c.pop_size - c.ri)

        for i in range(0, c.pop_size):
            values.append(self.p[i].fitness)
            ages[i] = self.p[i].age

            if i < c.pop_size - c.ri:
                diffs[i] = self.p[i].fitness - self.p[i].parent_fitness

        return np.max(values), np.min(values), np.median(values), np.average(values), diffs, ages

    def merge_parents(self, parents):
        """Merges a parent population into the current population, dropping any individual that doesn't
        make the cut according to NSGA-II

        :param parents: <Population> the parent population
        :return:
        """
        individuals_list = np.ndarray(shape=(2*c.pop_size, 2), dtype=float)

        # build list from parents and children
        # fitness needs to be flipped (*-1) because the ranker treats it as a cost
        for i in range(0, c.pop_size):
            individuals_list[i] = [parents.p[i].age, -1 * parents.p[i].fitness]

        for i in range(0, c.pop_size):
            individuals_list[i+c.pop_size] = [self.p[i].age, -1 * self.p[i].fitness]

        ranks = pareto_ranker.get_ranks(individuals_list)

        # select individuals based on pareto rank
        result_idx = np.array([], dtype=int)
        n_ids = 0
        last_rank = 0

        for i in range(1, 100000):
            last_rank = i
            tmp = np.where(ranks == i)[0]
            n_ids_tmp = len(tmp)

            if n_ids_tmp + n_ids <= c.pop_size:
                result_idx = np.append(result_idx, tmp)
                n_ids += n_ids_tmp
            else:
                break

        # if not filled exactly, use crowding distance with the last pareto rank to fill up
        if n_ids < c.pop_size:
            # get the indices of the rank for which crowding distance must be determined
            crowding_idx = np.where(ranks == last_rank)[0]

            # create a list of the crowding individuals only
            crowding_pop = individuals_list[crowding_idx]

            # get the crowding distances
            crowding_distances = pareto_ranker.get_crowding_distances(crowding_pop)

            crowding_result_idx = np.array([], dtype=int)

            for i in range(0, c.pop_size-n_ids):
                tmp_id = np.where(crowding_distances == np.max(crowding_distances))[0][0]
                selected = crowding_idx[tmp_id]

                # add selected index to crowding results
                crowding_result_idx = np.append(crowding_result_idx, selected)

                # delete from selection to prevent selecting the same max
                crowding_distances = np.delete(crowding_distances, tmp_id)
                crowding_idx = np.delete(crowding_idx, tmp_id)

            # append results of crowding to results of ranking
            result_idx = np.append(result_idx, crowding_result_idx)

        # copy selected individuals
        result_individuals = {}

        for i in range(0, c.pop_size):
            # ids < pop size are from parents, ids > pop size are from children (self)
            if result_idx[i] < c.pop_size:
                tmp_individual = copy.deepcopy(parents.p[result_idx[i]])
            else:
                tmp_id_c = result_idx[i] % c.pop_size
                tmp_individual = copy.deepcopy(self.p[tmp_id_c])

            result_individuals[i] = tmp_individual

        self.p = result_individuals

    def mutate(self):
        """mutates every individual in the population

        :return:
        """
        for i in self.p:
            self.p[i].mutate()

    def select_co_parent(self, parent, skip, other, preferences=None):
        """ Selects a co parent for a given parent from other

        :param parent: <int> id of parent in self.p
        :param skip: <array of ints>
        :param other: <POPULATION>
        :param preferences: <dict> preferences of the parent if needed for selection
        :return: <int>
        """
        skip = [parent] + skip
        return self.winner_of_tournament(other, tournament_type=1, skip=skip, preferences=preferences)

    def randomize(self):
        """Generates random values for each individual of the population

        :return:
        """
        for i in self.p:
            self.p[i].random_init()

    def winner_of_tournament(self, other, tournament_type=None, skip=None, preferences=None):
        """Selects a parent by selecting n (=tournament size) randomly and returning the best

        :param other: <POPULATION>
        :param tournament_type: <int> 0 for parent selection, 1 for co-parent selection
        :param skip: optional, <array of ints> ints that should not be selected
        :param preferences: <dict> dictionary holding the preference value for each measure
        :return: <int> id of winning parent
        """
        if tournament_type is None:
            print "ERROR: tournament type not specified"
            return False

        result = -1

        if skip is None:
            skip = []

        pool = np.arange(other.pop_size)

        pool = np.setdiff1d(pool, skip)

        # select ids for the tournament
        selected_is = random.sample(pool, c.tournament_size)

        # depending on the co_select_method constant a different technique will be used
        if c.co_select_method == 0 or tournament_type == 0:
            # based on fitness
            max_i = -99999
            max_f = -99999
            for i in selected_is:
                if other.p[i].fitness > max_f:
                    max_f = other.p[i].fitness
                    max_i = i

            result = max_i
        elif c.co_select_method == 1:
            # based on preferences
            max_i = -99999
            max_score = -99999

            for i in selected_is:
                tmp_score = other.p[i].get_preference_score(preferences, self.measure_extremes)
                if tmp_score > max_score:
                    max_score = tmp_score
                    max_i = i

            result = max_i
        elif c.co_select_method == 2:
            # based on fitness with penalty
            max_i = -99999
            max_f = -99999

            min_val = self.measure_extremes[c.penalty_measure][0]
            max_val = self.measure_extremes[c.penalty_measure][1]

            for i in selected_is:
                tmp_fitness = other.p[i].fitness

                norm_val = (other.p[i].genome['measures'][c.penalty_measure] - min_val) / (max_val - min_val)

                reduction = c.penalty_impact * norm_val

                tmp_fitness -= reduction

                if tmp_fitness > max_f:
                    max_f = other.p[i].fitness
                    max_i = i

            result = max_i
        else:
            print "ERROR: unknown co_select_method:", c.co_select_method
        
        return result

    def get_best(self, play=False):
        """Plays the individual with the highest fitness from the population

        :return:
        """
        best_f = -999999
        best_i = -1
        for i in self.p:
            if self.p[i].fitness > best_f:
                best_i = i
                best_f = self.p[i].fitness

        if play:
            self.p[best_i].start_evaluation(False, False, wait=True)

        tmp_genome = copy.deepcopy(self.p[best_i].genome)
        tmp_genome['extras'] = {}
        tmp_genome['extras']['found_best_f'] = best_f
        tmp_genome['extras']['found_best_i'] = best_i
        tmp_genome['extras']['actual_f'] = self.p[best_i].fitness
        tmp_genome['extras']['actual_i'] = self.p[best_i].id

        return tmp_genome, best_f

if __name__ == "__main__":
    testPopulation = Population(c.pop_size)
    testPopulation.initialize()
    testPopulation.randomize()
    testPopulation.evaluate(False, True)

    children = Population(c.pop_size)
    children.fill_from(testPopulation, print_parents=True)
    children.evaluate(False, True)
