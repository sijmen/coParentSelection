# created by Sijmen van der Willik
# framework used is pyrosim
# to run this code:
 - install pyrosim from https://github.com/ccappelle/pyrosim
 - place the python files in the pyrosim repo (clone repo)
 - run the desired algorithm
 
note: tested with pyrosim version (commit sha on master branch):
 76d97d94378b78724436b955801a9378f7166285

 i.e. python coParentSelection_base.py