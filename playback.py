# Created by Sijmen van der Willik
# 22-06-17 16:47

import sys
import os
import glob
import json
import pickle
import matplotlib.pyplot as plt
from individual import Individual

"""Usage:

playback.py
    runs the most recent file from output

playback.py [filename]
    runs the file specified by filename

playback.py [<int> n]
    runs the n most recent file, i.e. 1 is the newest, 2 is the second newest
"""

recency_n = 1

try:
    recency_n = int(sys.argv[1])
except:
    pass


class Playback:
    def __init__(self, n, search_dir="output", skip_stats=False):
        dir_list = list(os.listdir(search_dir))
        dir_list = [x for x in dir_list if '.sh' not in x]
        dir_list.sort()
        self.dir_list = dir_list

        self.capture = False
        self.constants = {}
        self.genomes = []
        self.statistics = {}
        self.eval_time = -1
        self.pop_size = -1
        self.generations = -1
        self.seed = -1
        self.runtime = -1
        self.dir = ""
        self.load(n, search_dir=search_dir, skip_stats=skip_stats)

    def load(self, n, search_dir="output", skip_stats=False):
        folder_name = search_dir + "/" + self.dir_list[-n]
        print "using dir:", self.dir_list[-n]
        self.dir = self.dir_list[-n]

        # load constants
        constants_file_name = max(glob.iglob(folder_name + '/constants*.txt'), key=os.path.getctime)
        f = open(constants_file_name, 'r')
        constants = f.readline()
        self.constants = json.loads(constants)

        # load genomes
        genomes_file_name = max(glob.iglob(folder_name + '/genomes*.txt'), key=os.path.getctime)
        f = open(genomes_file_name, 'r')
        genomes = f.readlines()
        genomes = "".join(genomes)
        self.genomes = pickle.loads(genomes)

        # load other
        other_file_name = max(glob.iglob(folder_name + '/other*.txt'), key=os.path.getctime)
        f = open(other_file_name, 'r')
        self.runtime = float(f.readline()[8:])
        self.seed = int(f.readline()[5:])

        # load statistics
        if not skip_stats:
            statistics_file_name = max(glob.iglob(folder_name + '/statistics*.txt'), key=os.path.getctime)
            f = open(statistics_file_name, 'r')
            statistics = f.readlines()
            statistics = "".join(statistics)
            self.statistics = pickle.loads(statistics)

        self.eval_time = self.constants['eval_time']
        self.pop_size = self.constants['pop_size']
        self.generations = self.constants['num_gens']

        print "eval_time:", self.eval_time
        print "pop_size:", self.pop_size
        print "generations:", self.generations

    def eval(self, n):
        """ plays back an individual as saved in the genomes file

        :param n: index of the genome to be played
        :return:
        """
        playback = Individual(0)
        playback.genome = self.genomes[n]

        if self.capture:
            "Capturing frames is ON, playback may be slow"

        playback.start_evaluation(False, False, eval_time=self.eval_time, capture=self.capture)
        playback.compute_fitness()
        print "fitness:", playback.fitness

    def plot(self, export=False):
        """ plots the data as saved in the statistics file

        :return:
        """
        try:
            plt.plot(self.statistics['diffs'], 'c.')
            plt.plot(self.statistics['best'], 'g')
            plt.plot(self.statistics['worst'], 'r')
            plt.plot(self.statistics['median'], 'b--')
            plt.plot(self.statistics['average'], 'b*')
            plt.ylabel('some numbers')
            if export:
                no = 0
                if self.constants['use_aggression']:
                    no = 8
                if self.constants['use_size_variation'] and not self.constants['use_aggression']:
                    no = 7
                if not self.constants['use_size_variation']:
                    no = 6
                filename = 'exp' + str(no) + '_' + str(self.seed)
                plt.savefig(filename)
            else:
                plt.show()
        except:
            print "ERROR: something is wrong with the loaded stats"

    def print_genome(self, n):
        """Shows the genome of the selected individual

        :param n: <int> can be negative, index of individual
        :return:
        """
        playback = Individual(0)
        playback.genome = self.genomes[n]

        playback.start_evaluation(False, True, eval_time=self.eval_time, capture=self.capture)
        playback.compute_fitness()

        for i in playback.genome['morphology']:
            print i

        print "fitness:", playback.fitness

    def capture_on(self):
        self.capture = True
        print "Capturing frames is now turned ON"

    def capture_off(self):
        self.capture = False
        print "Capturing frames is now turned OFF"


if __name__ == "__main__":
    playback_obj = Playback(recency_n)
    playback_obj.eval(-1)
