# Created by Sijmen van der Willik
# 15-6-17 17:50

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import numpy as np
import constants as c
from robot import Robot
from individual import Individual
import pyrosim


class TestIndividual(object):
    def __init__(self):
        pass

    def test_create_measures(self):
        test_individual = Individual(0)

        test_individual.fill_model(model=2, use_size_variation=False)
        test_individual.randomize_sinusoids()

        test_individual.start_evaluation(False, True)

        test_individual.compute_fitness()
        assert test_individual.genome['measures']['length'] == 2 * c.L
        assert test_individual.genome['measures']['width'] == 2 * c.L
        if c.use_aggression:
            assert test_individual.genome['measures']['aggression'] == 1

    def test_generate_gauss(self):
        assert Individual.generate_gauss(100) == 1
        assert Individual.generate_gauss(-100) == 0

    def test_get_preference_score(self):
        # straight values
        test_individual = Individual(0)
        test_individual.random_init()
        test_individual.genome['measures']['length'] = 1
        test_individual.genome['measures']['width'] = 1
        test_individual.genome['measures']['aggression'] = 1

        preferences = {'length': 1, 'width': 1, 'aggression': 1}
        extremes = {'length': [0, 1], 'width': [0, 1], 'aggression': [0, 1]}

        result = test_individual.get_preference_score(preferences=preferences, extremes=extremes)

        assert result == 3

        # requires normalizing
        test_individual.genome['measures']['length'] = 2
        test_individual.genome['measures']['width'] = 2
        test_individual.genome['measures']['aggression'] = 2

        preferences = {'length': 1, 'width': 1, 'aggression': 1}
        extremes = {'length': [0, 2], 'width': [0, 2], 'aggression': [0, 2]}

        result = test_individual.get_preference_score(preferences=preferences, extremes=extremes)

        assert result == 3

        # requires normalizing and preference scaling
        test_individual.genome['measures']['length'] = 2
        test_individual.genome['measures']['width'] = 2
        test_individual.genome['measures']['aggression'] = 2

        preferences = {'length': 1, 'width': 0, 'aggression': 1}
        extremes = {'length': [0, 2], 'width': [0, 2], 'aggression': [0, 2]}

        result = test_individual.get_preference_score(preferences=preferences, extremes=extremes)

        assert result == 2

        test_individual.genome['measures']['length'] = 2
        test_individual.genome['measures']['width'] = 2
        test_individual.genome['measures']['aggression'] = 2

        preferences = {'length': 1, 'width': 0, 'aggression': -1}
        extremes = {'length': [0, 4], 'width': [0, 2], 'aggression': [0, 2]}

        result = test_individual.get_preference_score(preferences=preferences, extremes=extremes)

        assert result == -0.5

    def test_inherit(self):
        # testing length changes
        test_i_0 = Individual(0)
        test_i_1 = Individual(1)

        test_i_0.fill_model(use_size_variation=False)
        test_i_1.fill_model(use_size_variation=False)

        test_i_1.genome['morphology'][0]['size'] = 2.0

        test_i_0.start_evaluation(False, True)
        test_i_1.start_evaluation(False, True)

        test_i_0.compute_fitness()
        test_i_1.compute_fitness()

        # assert difference because float inaccuracy
        assert abs(test_i_0.genome['measures']['length'] - 2 * c.L) < 0.001
        assert abs(test_i_0.genome['measures']['width'] - 2 * c.L) < 0.001
        assert abs(test_i_1.genome['measures']['length'] - 3 * c.L) < 0.001
        assert abs(test_i_1.genome['measures']['width'] - 2 * c.L) < 0.001

        # build Individual that will inherit
        test_i_2 = Individual(2)
        inherit_result = test_i_2.inherit(test_i_0.genome, test_i_1.genome, method=0)
        assert inherit_result is not False
        test_i_2.start_evaluation(False, True)
        test_i_2.compute_fitness()

        assert abs(test_i_2.genome['measures']['length'] - 2 * c.L - c.size_inherit_impact * c.L) < 0.001

        # WARNING: 1.0 is directly derived from the l_mp value in individual.py, inherit() function
        # change this value accordingly
        assert abs(test_i_2.genome['morphology'][0]['size'] - 1 - c.size_inherit_impact * 1.0) < 0.001
        assert abs(test_i_2.genome['morphology'][1]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][2]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][3]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][4]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][5]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][6]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][7]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][8]['size'] - 1) < 0.001

        # testing width changes
        test_i_0 = Individual(0)
        test_i_1 = Individual(1)

        test_i_0.fill_model(use_size_variation=False)
        test_i_1.fill_model(use_size_variation=False)

        test_i_1.genome['morphology'][1]['size'] = 2.0

        test_i_0.start_evaluation(False, True)
        test_i_1.start_evaluation(False, True)

        test_i_0.compute_fitness()
        test_i_1.compute_fitness()

        # assert difference because float inaccuracy
        assert abs(test_i_0.genome['measures']['length'] - 2 * c.L) < 0.001
        assert abs(test_i_0.genome['measures']['width'] - 2 * c.L) < 0.001
        assert abs(test_i_1.genome['measures']['length'] - 2 * c.L) < 0.001
        assert abs(test_i_1.genome['measures']['width'] - 3 * c.L) < 0.001

        # build Individual that will inherit
        test_i_2 = Individual(2)
        inherit_result = test_i_2.inherit(test_i_0.genome, test_i_1.genome, method=0)
        assert inherit_result is not False
        test_i_2.start_evaluation(False, True)
        test_i_2.compute_fitness()

        assert abs(test_i_2.genome['measures']['width'] - 2 * c.L - c.size_inherit_impact * c.L) < 0.001

        # WARNING: 0.5 is directly derived from the w_mp value in individual.py, inherit() function
        # change this value accordingly
        assert abs(test_i_2.genome['morphology'][0]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][1]['size'] - 1 - c.size_inherit_impact * 0.5) < 0.001
        assert abs(test_i_2.genome['morphology'][2]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][3]['size'] - 1 - c.size_inherit_impact * 0.5) < 0.001
        assert abs(test_i_2.genome['morphology'][4]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][5]['size'] - 1 - c.size_inherit_impact * 0.5) < 0.001
        assert abs(test_i_2.genome['morphology'][6]['size'] - 1) < 0.001
        assert abs(test_i_2.genome['morphology'][7]['size'] - 1 - c.size_inherit_impact * 0.5) < 0.001
        assert abs(test_i_2.genome['morphology'][8]['size'] - 1) < 0.001


