# Created by Sijmen van der Willik
# 14-6-17 14:27

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import constants as c


class TestConstants(object):
    def __init__(self):
        pass

    def test_parent_numbers(self):
        assert c.pop_size == c.pa * c.co * c.ch + c.ri

    def test_size_variation(self):
        assert 0 <= c.size_variation < 1

    def test_mutate_rate(self):
        assert c.mutate_rate >= 0

    def test_tournament_size(self):
        assert c.tournament_size <= c.pop_size - c.co
