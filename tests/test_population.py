# Created by Sijmen van der Willik
# 23-8-17 15:16

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import numpy as np
import constants as c
from population import Population
import pyrosim


class TestPopulation(object):
    def __init__(self):
        pass

    def test_initialize(self):
        parents = Population(5)
        parents.initialize()

    def test_get_measure_extremes(self):
        parents = Population(c.pop_size)
        parents.initialize()
        parents.randomize()

        # impossible values are being used to ensure values are always the extremes
        parents.p[0].genome['measures']['length'] = -0.001
        parents.p[4].genome['measures']['length'] = 2.000
        parents.p[0].genome['measures']['width'] = -0.001
        parents.p[4].genome['measures']['width'] = 2.000
        if c.use_aggression:
            parents.p[0].genome['measures']['aggression'] = -0.001
            parents.p[4].genome['measures']['aggression'] = 1.000
        parents.get_measure_extremes()
        print parents.measure_extremes
        assert parents.measure_extremes['length'][0] == -0.001
        assert parents.measure_extremes['length'][1] == 2.000
        assert parents.measure_extremes['width'][0] == -0.001
        assert parents.measure_extremes['width'][1] == 2.000

        if c.use_aggression:
            assert parents.measure_extremes['aggression'][0] == -0.001
            assert parents.measure_extremes['aggression'][1] == 1.000

    def test_scoring(self):
        # TODO
        pass
