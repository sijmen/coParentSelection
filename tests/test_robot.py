# Created by Sijmen van der Willik
# 26-5-17 17:06

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

import numpy as np
import constants as c
from robot import Robot
import pyrosim


class TestRobot(object):
    def __init__(self):
        pass

    def test_create_robot_in_sim(self):
        genome = {
            'wts': 0,
            'morphology': {},
            'tau': 1,
            'preferences': {}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        Robot(sim, genome)
        sim.start()

    def test_get_own_transform(self):
        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [1, 0, 0], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([0.5 * c.L, 0, 0])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 1, 0], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([0, 0.5 * c.L, 0])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 0, 1], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([0, 0, 0.5 * c.L])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [-1, 0, 0], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([-0.5 * c.L, 0, 0])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, -1, 0], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([0, -0.5 * c.L, 0])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 0, -1], 'rgb': [1, 1, 1]}
        result1 = Robot.get_own_transform(limb)
        compare = np.array([0, 0, -0.5 * c.L])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

    def test_get_attach_point(self):
        genome = {
            'wts': 0,
            'morphology': [{'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 1, 0],
                            'joint_dir': [1, 0, 0], 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}}],
            'tau': 1,
            'preferences': {'sizeX': 1, 'sizeY': 0}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        robot = Robot(sim, genome)
        limb = {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [-1, 0, 0], 'rgb': [1, 1, 1]}
        result1 = robot.get_attach_point(limb)
        compare = np.array([0, 0.5 * c.L, c.R])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

        limb = {'id': 1, 'parent': 0, 'side': 0, 'size': 1, 'dir': [-1, 0, 0], 'rgb': [1, 1, 1]}
        result1 = robot.get_attach_point(limb)
        compare = np.array([0, -0.5 * c.L, c.R])
        assert result1[0] == compare[0]
        assert result1[1] == compare[1]
        assert result1[2] == compare[2]

    def test_add_limb(self):
        genome = {
            'wts': 0,
            'morphology': [{'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 1, 0],
                            'joint_dir': [1, 0, 0], 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}}],
            'tau': 1,
            'preferences': {'sizeX': 1, 'sizeY': 0}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        robot = Robot(sim, genome)
        assert 1 in robot.limb_ids
        assert robot.morphology[1][1] is not None
        assert robot.morphology[1][1]['x'] == 0
        assert robot.morphology[1][0]['y'] == 0.5 * c.L
        assert robot.morphology[1][1]['y'] == 1.5 * c.L
        sim.start()

    def test_add_multiple_limbs(self):
        genome = {
            'wts': 0,
            'morphology': [
                {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 1, 0],
                 'joint_dir': [1, 0, 0], 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}},
                {'id': 2, 'parent': 1, 'side': 1, 'size': 1, 'dir': [0, 1, 0],
                 'joint_dir': [1, 0, 0], 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}},
                {'id': 3, 'parent': 2, 'side': 1, 'size': 1, 'dir': [1, 0, 0],
                 'joint_dir': [1, 0, 0], 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}}
            ],
            'tau': 1,
            'preferences': {'sizeX': 1, 'sizeY': 0}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        robot = Robot(sim, genome)
        assert 2 in robot.limb_ids
        assert robot.morphology[1][1] is not None
        assert robot.morphology[2][1]['x'] == 0
        assert robot.morphology[2][1]['y'] == 2.5 * c.L
        assert robot.morphology[3][1]['x'] == c.L
        assert robot.morphology[3][1]['y'] == 2.5 * c.L
        sim.start()

    def test_place_on_the_ground(self):
        genome = {
            'wts': 0,
            'morphology': [
                {'id': 1, 'parent': 0, 'side': 1, 'size': 1, 'dir': [0, 0, -1],
                 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}},
            ],
            'tau': 1,
            'preferences': {'sizeX': 1, 'sizeY': 0}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        robot = Robot(sim, genome)
        robot.place_on_ground()
        assert robot.morphology[0][0]['z'] == c.L + c.R

        genome = {
            'wts': 0,
            'morphology': [
                {'id': 1, 'parent': 0, 'side': 1, 'size': 2, 'dir': [0, 0, -1],
                 'sin': {'offset': 0.5, 'amp': 2, 'period': 3}},
            ],
            'tau': 1,
            'preferences': {'sizeX': 1, 'sizeY': 0}
        }
        sim = pyrosim.Simulator(play_paused=False, eval_time=200, play_blind=True)
        robot = Robot(sim, genome)
        robot.place_on_ground()
        assert robot.morphology[0][0]['z'] == 2 * c.L + c.R
