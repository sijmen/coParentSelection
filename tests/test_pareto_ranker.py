# Created by Sijmen van der Willik
# 15-8-17 12:36

import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))

from pareto_ranker import *
import numpy as np


class TestParetoRanker:
    def __init__(self):
        pass

    def test_get_ranks(self):
        # small square
        pop = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        result = get_ranks(pop)
        compare_arr = [1, 2, 2, 3]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # small square + some noise
        pop = np.array([[0, 0], [0, 1], [1, 0], [1, 1], [0, 0.5], [1, 1.5], [0.5, 0.5]])
        result = get_ranks(pop)
        compare_arr = [1, 3, 2, 4, 2, 5, 3]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # single line
        pop = np.array([[0, 0], [0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6]])
        result = get_ranks(pop)
        compare_arr = [1, 2, 3, 4, 5, 6, 7]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # single line with duplicates
        pop = np.array([[0, 0], [0, 1], [0, 1], [0, 2], [0, 3], [0, 3], [0, 4], [0, 5], [0, 6]])
        result = get_ranks(pop)
        compare_arr = [1, 2, 2, 3, 4, 4, 5, 6, 7]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]


class TestCrowdingDistances:
    def __init__(self):
        pass

    def test_get_crowding_distances(self):
        # two values, should give extremes
        pop = np.array([[0, 0], [0, 1]])
        result = get_crowding_distances(pop)
        compare_arr = [999999, 999999]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # three values, should give distance for middle one
        pop = np.array([[0, 0], [1, 1], [2, 2]])
        result = get_crowding_distances(pop)
        compare_arr = [999999, 2, 999999]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # three values, should give distance for middle one, requires normalizing
        pop = np.array([[0, 0], [10, 1], [20, 2]])
        result = get_crowding_distances(pop)
        compare_arr = [999999, 2, 999999]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # five values, requires normalizing, contains duplicate
        pop = np.array([[0, 0], [5, 0.5], [10, 1], [20, 2], [20, 2]])
        result = get_crowding_distances(pop)
        compare_arr = [999999, 1, 1.5, 1, 999999]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

        # five values, requires normalizing, contains duplicate
        pop = np.array([[0, 0], [5, 0.5], [10, 1], [20, 2], [20, 2]])
        result = get_crowding_distances(pop)
        compare_arr = [999999, 1, 1.5, 1, 999999]

        for i in range(0, len(result)):
            assert compare_arr[i] == result[i]

